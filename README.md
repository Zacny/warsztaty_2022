# Warsztaty 2022 #

Materiały na warsztaty 2022.

## Agenda

* Informacje o kierunkach i specjalnościach na UEKat
* Wprowadzenie do języka `python`
* Wprowadzenie do przetwarzania obrazów

## Materiały

1. [Prezentacja](https://slides.com/bognazacny/meritum_python_2122) (`Meritum_python_2122.html`) 
2. Notatniki Jupter (`Meritum_python_2122.ipynb`, `Meritum_python_2122_stud.ipynb`)
3. Obrazki (`most.jpg`, `pencils.jpg`)